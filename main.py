# pip install mysql-connector-python

import mysql.connector
from mysql.connector import Error
import os

# DB details
# db_host = "mysql"
# db_username = "root"
# db_password = "root"
# db_name = "test"
db_host = os.environ.get('DB_HOST')
db_username = os.environ.get('DB_USERNAME')
db_password = os.environ.get('DB_PASSWORD')
db_name = os.environ.get('DB_NAME')

# Function to test the database connection
def create_server_connection(host_name, user_name, user_password):
    connection = None
    try:
        connection = mysql.connector.connect(
            host=host_name,
            user=user_name,
            passwd=user_password
        )
        print("[Success]: Connected to database!")
    except Error as err:
        print(f"[Error]: '{err}'")

    return connection

# Calling the DB test function
connection = create_server_connection(db_host, db_username, db_password)
